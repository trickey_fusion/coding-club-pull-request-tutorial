# Pull Request Tutorial

This repo is for demonstrating pull requests

It consists of an overly simple calculator module and corresponding
tests, which starts off providing two "useful" functions, `add` and
`subtract`, as well as stubs for a bunch of other mathematical
functions.

Your job is to implement one of the stubbed functions.

## Pull Requests

1. Fork this repo
2. Make a new branch
3. Create a new file called `<your_name>.py` along with a
   corresponding test file `test_<your_name>.py`
4. Implement a simple maths function in the source file and some tests
   for it in the test file
    - e.g. multiply, divide, harmonic mean, `sin`, `cos`
    - Don't forget documentation!
5. Push your branch
6. Open a Pull Request and request someone as a reviewer
7. When someone requests you as a reviewer, read over the pull request
   to check that it is sensible, well implemented, has documentation
   and tests
8. Approve the PR, or request changes
