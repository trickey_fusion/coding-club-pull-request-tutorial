def add(a, b):
    """Add two numbers together

    """
    return a + b


def subtract(a, b):
    """Subtract one number from another

    """
    return a - b
